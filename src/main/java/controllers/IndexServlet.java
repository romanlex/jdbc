package controllers;

import models.Student;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        try {
            ArrayList<Student> studentlist = new ArrayList<Student>();
            Student students = new Student();
            ResultSet rs = students.find();

            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }


            int i = 0;
            while (rs.next()) {
                i++;
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setFamily(rs.getString("family"));
                student.setPatronymic(rs.getString("patronymic"));
                studentlist.add(student);
            }

            req.setAttribute("students", studentlist);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/students.jsp");
            dispatcher.forward(req, resp);
        } catch (SQLException e) {
            req.setAttribute("error", e.getMessage());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/error.jsp");
            dispatcher.forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
