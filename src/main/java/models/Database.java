package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    public static final Database INSTANCE = new Database();
    public static Connection db;

    protected static final String dbname = "jdbc";
    protected static final String user = "root";
    protected static final String password = "dbadmin2015";

    public Database() {

    }

    public Connection newConnection() throws SQLException {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            System.out.println("Driver loading succes!");
            db = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + dbname + "?user="+user+"&password="+password);
            return db;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Connection getConnection() throws SQLException {
        if(db == null)
            return newConnection();
        else
            return db;
    }
}
