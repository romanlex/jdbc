package models;

import java.lang.reflect.Field;
import java.sql.*;
import java.text.ParseException;

abstract class Model {
    public String tablename;

    public void setSource(String tablename) {
        this.tablename = tablename;
    }

    public int add(Object obj) throws SQLException, IllegalAccessException, ParseException {
        Connection db = Database.INSTANCE.getConnection();
        String sqlQ = "INSERT INTO " + tablename + " (family, name, patronymic, birthdate) " +
                "Values (?,?,?,?)";
        PreparedStatement preparedStatement = db.prepareStatement(sqlQ);

        Field[] fields = obj.getClass().getDeclaredFields();

        int i = 1;
        for (Field field:
                fields) {

            field.setAccessible(true);
            Object value = field.get(obj);

            switch (field.getType().getSimpleName()) {
                case "String":
                    preparedStatement.setString(i, value.toString());
                    break;
                case "Date":
                    java.sql.Date sqlDate =  java.sql.Date.valueOf(value.toString());
                    preparedStatement.setDate(i, sqlDate);
                    break;
            }
            i++;
        }
        return preparedStatement.executeUpdate();
    }


    public ResultSet find() throws SQLException {
        Connection db = Database.INSTANCE.getConnection();
        Statement query = db.createStatement();
        ResultSet result = query.executeQuery("SELECT * FROM " + tablename);
        return result;
    }


    public ResultSet findFirst(Integer id) throws SQLException {
        Connection db = Database.INSTANCE.getConnection();
        Statement query = db.createStatement();
        ResultSet result = query.executeQuery("SELECT * FROM " + tablename + " WHERE id = " + id);
        return result;
    }

}
