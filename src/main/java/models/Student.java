package models;

public class Student extends Model {

    private static final String table = "students";

    private int id;
    private String name;
    private String family;
    private String patronymic;


    public Student() {
        setSource(table);
    }

    public Student(String name, String family, String patronymic) {
        this.name = name;
        this.family = family;
        this.patronymic = patronymic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

}
