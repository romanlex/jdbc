<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Error</title>
</head>
<body>
    <div class="wrapper">
        <div class="content">
            <h1>Error</h1>
            <h4 class="desc">
                <%= request.getAttribute("error") %>
            </h4>
        </div>
    </div>
</body>
</html>
