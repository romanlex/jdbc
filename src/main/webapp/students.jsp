<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="models.Student" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Students</title>
</head>
<body>
<div class="wrapper">
    <div class="content">
        <h1>Students list from database</h1>
        <br>
        <table border="1" style="min-width: 60%">
            <thead style="font-weight: bold;">
                <tr>
                    <td>#id</td>
                    <td>name</td>
                    <td>family</td>
                    <td>patronymic</td>
                </tr>
            </thead>
            <tbody>
            <c:if test="${not empty students}">
                <c:forEach items="${students}" var="student">
                    <tr>
                        <td>
                            <c:if test="${not empty student.id}">
                                <c:out value="${student.id}"></c:out>
                            </c:if>
                            <c:if test="${empty student.id}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty student.name}">
                                <c:out value="${student.name}"></c:out>
                            </c:if>
                            <c:if test="${empty student.name}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty student.family}">
                                <c:out value="${student.family}"></c:out>
                            </c:if>
                            <c:if test="${empty student.family}">Не указано</c:if>
                        </td>
                        <td>
                            <c:if test="${not empty student.patronymic}">
                                <c:out value="${student.patronymic}"></c:out>
                            </c:if>
                            <c:if test="${empty student.patronymic}">Не указано</c:if>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
            <c:if test="${empty students}">
                <tr>
                    <td colspan="5">
                        Студентов не найдено
                    </td>
                </tr>
            </c:if>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
